#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

$log = new \FightClub\Infrastructure\Console\Log();
$rounds = 20;

$application = new \FightClub\Infrastructure\Console\Fight(
    new \FightClub\Domain\Model\Application\FightModel(
        new \FightClub\Domain\Model\Fighter\Hero('Orderus'),
        new \FightClub\Domain\Model\Fighter\Beast('Random Beast'),
        $rounds
    ),
    new \FightClub\Infrastructure\Console\Log()
);

$application->startFight();

exit();
