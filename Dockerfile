FROM php:7.1-apache

ADD  . /var/www/html/
WORKDIR /var/www/html/

RUN apt-get update && apt-get install -y \
    htop \
    vim \
    git

RUN apt-get install -y zlib1g-dev \
    && docker-php-ext-install zip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install --no-interaction

RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN echo "xdebug.remote_enable=1" >> /usr/local/etc/php/php.ini


