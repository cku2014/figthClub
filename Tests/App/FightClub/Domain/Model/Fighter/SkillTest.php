<?php
/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/16/17
 * Time: 8:11 PM.
 */

namespace FightClub\Domain\Model\Fighter;


use FightClub\Domain\Model\Skill\Skill;
use PHPUnit\Framework\TestCase;

class SkillTest extends TestCase
{
    public function testSkillWithValidData()
    {
        $name = 'skill';
        $chance = 10;
        $type = 'offensive';

        $a = new Skill($name, $chance, $type);

        $this->assertEquals($a->getName(), $name);
        $this->assertEquals($a->getChance(), $chance);
        $this->assertEquals($a->getType(), $type);
    }
}
