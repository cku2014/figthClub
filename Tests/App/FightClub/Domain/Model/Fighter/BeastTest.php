<?php
/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/16/17
 * Time: 8:28 PM.
 */

namespace FightClub\Domain\Model\Fighter;

use FightClub\Domain\Model\Fighter\Beast;
use PHPUnit\Framework\TestCase;

class BeastTest extends TestCase
{
    protected $hero;

    public function setUp()
    {
        $name = 'Nobil';
        $health = 50;
        $strength = 50;
        $defense = 50;
        $speed = 50;
        $luck = 60;

        $this->hero = new Beast($name);
        $this->hero->setHealth($health);
        $this->hero->setStrength($strength);
        $this->hero->setDefense($defense);
        $this->hero->setSpeed($speed);
        $this->hero->setLuck($luck);
    }

    public function testCreateBeastWithValidData()
    {
        $name = 'Nobil';
        $health = 50;
        $strength = 50;
        $defense = 50;
        $speed = 50;
        $luck = 60;

        $this->assertEquals($this->hero->getName(), $name);
        $this->assertEquals($this->hero->getHealth(), $health);
        $this->assertEquals($this->hero->getStrength(), $strength);
        $this->assertEquals($this->hero->getDefense(), $defense);
        $this->assertEquals($this->hero->getSpeed(), $speed);
        $this->assertEquals($this->hero->getLuck(), $luck);
    }
}
