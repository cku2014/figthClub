<?php
/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/16/17
 * Time: 8:28 PM.
 */

namespace FightClub\Domain\Model\Fighter;

use FightClub\Domain\Model\Fighter\Hero;
use PHPUnit\Framework\TestCase;

class HeroTest extends TestCase
{
    protected $hero;

    public function setUp()
    {
        $name = 'Orderus';
        $health = 50;
        $strength = 50;
        $defense = 50;
        $speed = 50;
        $luck = 60;

        $this->hero = new Hero($name);
        $this->hero->setHealth($health);
        $this->hero->setStrength($strength);
        $this->hero->setDefense($defense);
        $this->hero->setSpeed($speed);
        $this->hero->setLuck($luck);
    }

    public function testCreateHeroWithValidData()
    {
        $name = 'Orderus';
        $health = 50;
        $strength = 50;
        $defense = 50;
        $speed = 50;
        $luck = 60;

        $this->assertEquals($this->hero->getName(), $name);
        $this->assertEquals($this->hero->getHealth(), $health);
        $this->assertEquals($this->hero->getStrength(), $strength);
        $this->assertEquals($this->hero->getDefense(), $defense);
        $this->assertEquals($this->hero->getSpeed(), $speed);
        $this->assertEquals($this->hero->getLuck(), $luck);
    }

    public function testFighterStats()
    {
        $this->assertArrayHasKey('health', $this->hero->getFighterStats());
        $this->assertArrayHasKey('strength', $this->hero->getFighterStats());
        $this->assertArrayHasKey('defense', $this->hero->getFighterStats());
        $this->assertArrayHasKey('speed', $this->hero->getFighterStats());
        $this->assertArrayHasKey('luck', $this->hero->getFighterStats());
    }

    public function testHeroHasSkills()
    {
        self::assertInternalType('array', $this->hero->getSkills());
    }

    public function testHeroHasSkillsTrue()
    {
        self::assertTrue($this->hero->hasSkills());
        self::assertInternalType('boolean', $this->hero->hasSkills());
    }
}
