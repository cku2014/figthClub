<?php
/**
 * Created by PhpStorm.
 * User: cristian.tanase
 * Date: 3/26/18
 * Time: 10:23 PM.
 */

class FightModelTest extends \PHPUnit\Framework\TestCase
{
    private $hero;
    private $beast;
    private $rounds;
    private $fightModel;

    public function setUp()
    {
        $this->hero = new \FightClub\Domain\Model\Fighter\Hero('Orderus');
        $this->beast = new \FightClub\Domain\Model\Fighter\Beast('Beast');
        $this->rounds = 20;
        $this->fightModel = new \FightClub\Domain\Model\Application\FightModel(
            $this->hero,
            $this->beast,
            $this->rounds
            );
    }

    public function testSetAttacker()
    {
        $this->fightModel->setAttacker($this->hero);
        self::assertInstanceOf(\FightClub\Domain\Model\Fighter\Hero::class, $this->fightModel->getAttacker());
        self::assertNotInstanceOf(\FightClub\Domain\Model\Fighter\Beast::class, $this->fightModel->getAttacker());
    }

    public function testSetDefender()
    {
        $this->fightModel->setDefender($this->beast);
        self::assertInstanceOf(\FightClub\Domain\Model\Fighter\Beast::class, $this->fightModel->getDefender());
        self::assertNotInstanceOf(\FightClub\Domain\Model\Fighter\Hero::class, $this->fightModel->getDefender());
    }



}
