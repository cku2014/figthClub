<?php
/**
 * Created by PhpStorm.
 * User: cristian.tanase
 * Date: 3/23/18
 * Time: 3:06 PM.
 */

use PHPUnit\Framework\TestCase;

class FightTest extends TestCase
{
    private $log;
    private $hero;
    private $beast;
    private $fight;
    private $attacker;
    private $defender;
    private $fightModel;


    public function setUp()
    {

        $this->hero = new \FightClub\Domain\Model\Fighter\Hero('Orderus');
        $this->beast = new \FightClub\Domain\Model\Fighter\Beast('Beast');
        $this->log = new \FightClub\Infrastructure\Console\Log();
        $this->fightModel = new \FightClub\Domain\Model\Application\FightModel($this->hero, $this->beast, 20);

        $this->fight = new \FightClub\Infrastructure\Console\Fight($this->fightModel, $this->log);
        $this->fightModel->setDefender($this->beast);
        $this->fightModel->setAttacker($this->hero);

        $this->attacker = $this->hero;
        $this->defender = $this->beast;
    }

    public function testGetChance()
    {
        self::assertInternalType('int', $this->fight->getChance());
        self::assertLessThanOrEqual(100, $this->fight->getChance());
        self::assertGreaterThan(0, $this->fight->getChance());
    }

    public function testCheckWinner()
    {
        self::assertTrue($this->fight->checkWinner($this->attacker, $this->defender) instanceof \FightClub\Domain\Model\Fighter\FighterInterface || $this->fight->checkWinner($this->attacker, $this->defender) === null);

    }

    public function testBattleIsActiveTrue()
    {
        self::assertTrue($this->fight->battleIsActive());
        self::assertInternalType('bool', $this->fight->battleIsActive());
    }

    public function testSwapFighters()
    {
        $this->fight->swapFighters($this->hero, $this->beast);

        self::assertInstanceOf(\FightClub\Domain\Model\Fighter\Beast::class, $this->fightModel->getAttacker());
        self::assertInstanceOf(\FightClub\Domain\Model\Fighter\Hero::class, $this->fightModel->getDefender());
    }

    public function testSetHeroAsAttackerByLuck()
    {

        $this->hero->setLuck(100);
        $this->beast->setLuck(50);

        $this->fight->setAttackerOrderByLuck($this->hero, $this->beast);

        self::assertInstanceOf(\FightClub\Domain\Model\Fighter\Hero::class, $this->fightModel->getAttacker());
        self::assertInstanceOf(\FightClub\Domain\Model\Fighter\Beast::class, $this->fightModel->getDefender());
    }

    public function testSetHeroAsDefenderByLuck()
    {

        $this->hero->setLuck(50);
        $this->beast->setLuck(100);

        $this->fight->setAttackerOrderByLuck($this->hero, $this->beast);

        self::assertInstanceOf(\FightClub\Domain\Model\Fighter\Hero::class, $this->fightModel->getDefender());
        self::assertInstanceOf(\FightClub\Domain\Model\Fighter\Beast::class, $this->fightModel->getAttacker());
    }

    public function testCalculateDamage()
    {
        $this->hero->setStrength(50);
        $this->beast->setDefense(40);

        $this->fightModel->setAttacker($this->hero);
        $this->fightModel->setDefender($this->beast);


        self::assertGreaterThan(0, $this->fight->calculateDamage());
        self::assertLessThanOrEqual(20, $this->fight->calculateDamage());
    }
}
