<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/12/17
 * Time: 10:18 PM.
 */

namespace FightClub\Domain\Model\Skill;


/**
 * Class RapidStrikeSkill
 *
 * Rapid strike: Strike twice while it’s his turn to attack; there’s a 10% chance he’ll use
 * this skill every time he attacks
 *
 * @package FightClub\Domain\Model
 */
class RapidStrikeSkill extends Skill
{
    public function __construct(string $name, int $chance)
    {
        $type = SKILL::OFFENSIVE;
        parent::__construct($name, $chance, $type);
    }
}
