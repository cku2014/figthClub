<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/12/17
 * Time: 10:41 PM.
 */

namespace FightClub\Domain\Model\Skill;


/**
 * Class MagicShieldSkill
 * @package FightClub\Domain\Model\Skill
 */
class MagicShieldSkill extends Skill
{
    /**
     * MagicShieldSkill constructor.
     * @param string $name
     * @param int $chance
     */
    public function __construct(string $name, int $chance)
    {
        $type = SKILL::DEFENSIVE;
        parent::__construct($name, $chance, $type);
    }

    /**
     * @param float $damage
     * @return float
     */
    public function calculateDamageApplied(float $damage): float
    {
        return $damage / 2;
    }
}
