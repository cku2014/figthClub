<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/12/17
 * Time: 10:04 PM.
 */

namespace FightClub\Domain\Model\Skill;


/**
 * Class Skill
 * @package FightClub\Domain\Model\Skill
 */
class Skill implements SkillInterface
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $type;
    /**
     * @var integer
     */
    protected $chance;

    /**
     * Skill constructor.
     * @param string $name
     * @param int $chance
     * @param string $type
     */
    public function __construct(string $name, int $chance, string $type)
    {
        $this->setName($name);
        $this->setChance($chance);
        $this->setType($type);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return SkillInterface
     */
    public function setName(string $name): SkillInterface
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return SkillInterface
     */
    public function setType(string $type): SkillInterface
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getChance(): int
    {
        return $this->chance;
    }

    /**
     * @param int $chance
     * @return SkillInterface
     */
    public function setChance(int $chance): SkillInterface
    {
        $this->chance = $chance;
        return $this;
    }

    public function calculateDamageApplied(float $damage): float
    {
        return $damage;
    }
}
