<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: cristian.tanase
 * Date: 3/23/18
 * Time: 12:20 PM.
 */

namespace FightClub\Domain\Model\Skill;


/**
 * Class BestStrikeSkill
 * @package FightClub\Domain\Model\Skill
 */
class BestStrikeSkill extends Skill
{
    /**
     * BestStrikeSkill constructor.
     * @param string $name
     * @param int $chance
     */
    public function __construct(string $name, int $chance)
    {
        $type = SKILL::OFFENSIVE;
        parent::__construct($name, $chance, $type);
    }

    /**
     * @param float $damage
     * @return float
     */
    public function calculateDamageApplied(float $damage): float
    {
        return $damage * 2;
    }
}
