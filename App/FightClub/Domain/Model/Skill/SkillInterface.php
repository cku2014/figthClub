<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/11/17
 * Time: 9:34 PM.
 */

namespace FightClub\Domain\Model\Skill;

/**
 * Interface SkillInterface
 * @package FightClub\Domain\Model\Skill
 */
interface SkillInterface
{

    const DEFENSIVE = 'defensive';

    const OFFENSIVE = 'offensive';


    /**
     * @return string
     */
    public function getName(): string;


    /**
     * @param string $name
     * @return SkillInterface
     */
    public function setName(string $name): SkillInterface;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @param string $type
     * @return SkillInterface
     */
    public function setType(string $type): SkillInterface;

    /**
     * @return int
     */
    public function getChance(): int;

    /**
     * @param int $chance
     * @return SkillInterface
     */
    public function setChance(int $chance): SkillInterface;


    /**
     * @param float $damage
     * @return float
     */
    public function calculateDamageApplied(float $damage): float;
}

