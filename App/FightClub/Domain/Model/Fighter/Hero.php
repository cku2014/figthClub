<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/11/17
 * Time: 8:54 PM.
 */

namespace FightClub\Domain\Model\Fighter;

use FightClub\Domain\Model\Skill\BestStrikeSkill;
use FightClub\Domain\Model\Skill\MagicShieldSkill;
use FightClub\Domain\Model\Skill\RapidStrikeSkill;

/**
 * Class Hero
 * @package FightClub\Domain\Model
 */
class Hero extends Fighter
{
    /**
     * Hero constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct();
        $this->setName($name);
        $this->setHealth(rand(70, 100));
        $this->setStrength(rand(70, 80));
        $this->setDefense(rand(45, 55));
        $this->setSpeed(rand(40, 50));
        $this->setLuck(rand(10, 30));

        $this->setSkills([
            new RapidStrikeSkill('RapidStrike', 10),
            new MagicShieldSkill('MagicShield', 20),
            new BestStrikeSkill('BestStrike', 15)
        ]);

    }

}
