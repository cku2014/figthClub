<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/11/17
 * Time: 8:55 PM.
 */

namespace FightClub\Domain\Model\Fighter;

/**
 * Class Beast
 * @package FightClub\Domain\Model\Fighter
 */
class Beast extends Fighter
{
    /**
     * Beast constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct();

        $this->setName($name);
        $this->setHealth(rand(60, 90));
        $this->setStrength(rand(60, 90));
        $this->setDefense(rand(40, 60));
        $this->setSpeed(rand(40, 60));
        $this->setLuck(rand(25, 40));
    }

}
