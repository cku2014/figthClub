<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/11/17
 * Time: 6:27 PM.
 */

namespace FightClub\Domain\Model\Fighter;

use FightClub\Domain\Model\Skill\SkillInterface;

/**
 * Class Fighter
 * @package FightClub\Domain\Model
 */
class Fighter implements FighterInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var float
     */
    protected $health;

    /**
     * @var int
     */
    protected $strength;

    /**
     * @var int
     */
    protected $defense;

    /**
     * @var int
     */
    protected $speed;

    /**
     * @var int
     */
    protected $luck;

    /**
     * @var array
     */
    protected $skills;

    /**
     * @var boolean
     */
    protected $alive;


    /**
     * Fighter constructor.
     */
    public function __construct()
    {
        $this->skills = array();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Fighter
     */
    public function setName(string $name): FighterInterface
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getHealth(): float
    {
        return $this->health;
    }

    /**
     * @param float $health
     * @return Fighter
     */
    public function setHealth(float $health): FighterInterface
    {
        $this->health = $health;
        return $this;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     * @return Fighter
     */
    public function setStrength(int $strength): FighterInterface
    {
        $this->strength = $strength;
        return $this;
    }

    /**
     * @return int
     */
    public function getDefense(): int
    {
        return $this->defense;
    }

    /**
     * @param int $defense
     * @return Fighter
     */
    public function setDefense(int $defense): FighterInterface
    {
        $this->defense = $defense;
        return $this;
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * @param int $speed
     * @return Fighter
     */
    public function setSpeed(int $speed): FighterInterface
    {
        $this->speed = $speed;
        return $this;
    }

    /**
     * @return int
     */
    public function getLuck(): int
    {
        return $this->luck;
    }

    /**
     * @param int $luck
     * @return Fighter
     */
    public function setLuck(int $luck): FighterInterface
    {
        $this->luck = $luck;
        return $this;
    }

    /**
     * @return array
     */
    public function getSkills(): array
    {
        return $this->skills;
    }

    /**
     * @param array $skills
     * @return Fighter
     */
    public function setSkills(array $skills): FighterInterface
    {
        $this->skills = $skills;
        return $this;
    }

    /**
     * @return array
     */
    public function getFighterStats()
    {
        return [
            'health' => $this->getHealth(),
            'strength' => $this->getStrength(),
            'defense' => $this->getDefense(),
            'luck' => $this->getLuck(),
            'speed' => $this->getSpeed()
        ];
    }

    /**
     * @return array
     */
    public function getDefensiveSkills(): array
    {
        $applySkills = [];
        if ($this->hasSkills()) {
            foreach ($this->skills as $skill) {
                if ($skill->getType() == SkillInterface::DEFENSIVE) {
                    $applySkills[] = $skill;
                }
            }
        }

        return $applySkills;
    }

    /**
     * @return array
     */
    public function getOffensiveSkills(): array
    {
        $applySkills = [];
        if ($this->hasSkills()) {
            foreach ($this->skills as $skill) {
                if ($skill->getType() == SkillInterface::OFFENSIVE) {
                    $applySkills[] = $skill;
                }
            }
        }

        return $applySkills;
    }

    public function hasSkills(): bool
    {
        return count($this->getSkills()) > 0;
    }

    public function isAlive(): bool
    {
        return $this->getHealth() > 0;
    }

    public function inflictDamage(float $damage): void
    {
        $this->health -= $damage;
    }
}

