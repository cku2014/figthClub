<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/11/17
 * Time: 9:34 PM.
 */

namespace FightClub\Domain\Model\Fighter;

/**
 * Interface FighterInterface
 * @package FightClub\Domain\Model\Fighter
 */
interface FighterInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return FighterInterface
     */
    public function setName(string $name): FighterInterface;

    /**
     * @return float
     */
    public function getHealth(): float;

    /**
     * @param float $health
     * @return FighterInterface
     */
    public function setHealth(float $health): FighterInterface;

    /**
     * @return int
     */
    public function getStrength(): int;

    /**
     * @param int $strength
     * @return FighterInterface
     */
    public function setStrength(int $strength): FighterInterface;

    /**
     * @return int
     */
    public function getDefense(): int;

    /**
     * @param int $defense
     * @return FighterInterface
     */
    public function setDefense(int $defense): FighterInterface;

    /**
     * @return int
     */
    public function getSpeed(): int;

    /**
     * @param int $speed
     * @return FighterInterface
     */
    public function setSpeed(int $speed): FighterInterface;

    /**
     * @return int
     */
    public function getLuck(): int;

    /**
     * @param int $luck
     * @return FighterInterface
     */
    public function setLuck(int $luck): FighterInterface;

    /**
     * @return bool
     */
    public function hasSkills(): bool;


    /**
     * @param array $skills
     * @return FighterInterface
     */
    public function setSkills(array $skills): FighterInterface;

    /**
     * @return bool
     */
    public function isAlive(): bool;

    /**
     * @param float $damage
     */
    public function inflictDamage(float $damage): void;

    /**
     * @return array
     */
    public function getOffensiveSkills(): array;

    /**
     * @return array
     */
    public function getDefensiveSkills(): array;
}
