<?php
/**
 * Created by PhpStorm.
 * User: cristian.tanase
 * Date: 3/26/18
 * Time: 9:06 PM.
 */

namespace FightClub\Domain\Model\Application;

use FightClub\Domain\Model\Fighter\FighterInterface;
use FightClub\Domain\Model\Fighter\Hero;
use FightClub\Domain\Model\Fighter\Beast;
class FightModel
{
    /**
     * @var int
     */
    public $rounds;

    /**
     * @var int
     */
    public $currentRound = 1;

    /**
     * @var FighterInterface
     */
    public $attacker = null;

    /**
     * @var FighterInterface
     */
    public $defender = null;

    /**
     * @var float
     */
    public $damageValue;

    /**
     * @var Hero
     */
    public $hero;

    /**
     * @var Beast
     */
    public $beast;

    /**
     * FightModel constructor.
     * @param Hero $hero
     * @param Beast $beast
     * @param int $rounds
     */
    public function __construct(Hero $hero, Beast $beast, int $rounds)
    {
        $this->setHero($hero);
        $this->setBeast($beast);
        $this->setRounds($rounds);
    }

    /**
     * @return int
     */
    public function getRounds(): int
    {
        return $this->rounds;
    }

    /**
     * @param int $rounds
     * @return FightModel
     */
    public function setRounds(int $rounds): FightModel
    {
        $this->rounds = $rounds;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentRound(): int
    {
        return $this->currentRound;
    }

    /**
     * @param int $currentRound
     * @return FightModel
     */
    public function setCurrentRound(int $currentRound): FightModel
    {
        $this->currentRound = $currentRound;
        return $this;
    }

    /**
     * @return FighterInterface|null
     */
    public function getAttacker():? FighterInterface
    {
        return $this->attacker;
    }

    /**
     * @param FighterInterface $attacker
     * @return FightModel
     */
    public function setAttacker(FighterInterface $attacker): FightModel
    {
        $this->attacker = $attacker;
        return $this;
    }

    /**
     * @return FighterInterface|null
     */
    public function getDefender():? FighterInterface
    {
        return $this->defender;
    }

    /**
     * @param FighterInterface $defender
     * @return FightModel
     */
    public function setDefender(FighterInterface $defender): FightModel
    {
        $this->defender = $defender;
        return $this;
    }

    /**
     * @return float
     */
    public function getDamageValue(): float
    {
        return $this->damageValue;
    }

    /**
     * @param float $damageValue
     * @return FightModel
     */
    public function setDamageValue(float $damageValue): FightModel
    {
        $this->damageValue = $damageValue;
        return $this;
    }

    /**
     * @return Hero
     */
    public function getHero(): Hero
    {
        return $this->hero;
    }

    /**
     * @param Hero $hero
     * @return FightModel
     */
    public function setHero(Hero $hero): FightModel
    {
        $this->hero = $hero;
        return $this;
    }

    /**
     * @return Beast
     */
    public function getBeast(): Beast
    {
        return $this->beast;
    }

    /**
     * @param Beast $beast
     * @return FightModel
     */
    public function setBeast(Beast $beast): FightModel
    {
        $this->beast = $beast;
        return $this;
    }
}
