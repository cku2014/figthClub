<?php
declare(strict_types=1);

namespace FightClub\Infrastructure\Console;
require_once __DIR__ . '/../../../../vendor/autoload.php';

use FightClub\Domain\Model\Application\FightModel;
use FightClub\Domain\Model\Fighter\FighterInterface;
use FightClub\Domain\Model\Skill\BestStrikeSkill;
use FightClub\Domain\Model\Skill\MagicShieldSkill;
use FightClub\Domain\Model\Skill\RapidStrikeSkill;
use FightClub\Domain\Model\Skill\SkillInterface;

/**
 * Class Fight
 * @package FightClub\Infrastructure\Console
 */
class Fight
{
    /**
     * @var FightModel
     */
    public $fight;

    /**
     * Fight constructor.
     * @param FightModel $fight
     * @param Log $log
     */
    public function __construct(FightModel $fight, Log $log)
    {
        $this->fight = $fight;
        $this->log = $log;
    }

    /**
     *
     */
    public function startFight(): void
    {
        $this->log->echoFighter($this->fight->getHero());
        $this->log->echoFighter($this->fight->getBeast());

        try {
            $this->setAttackerAndDefender($this->fight->getHero(), $this->fight->getBeast());
        } catch (\Exception $e) {
            echo "\n" . $e->getMessage();
            echo "\n Mai dam cu zarul 1 data";
            exit();
        }

        while ($this->battleIsActive()) {
            $this->log->echoRound($this->fight->getCurrentRound());
            $this->performAttack();

            $this->swapFighters($this->fight->getAttacker(), $this->fight->getDefender());
            $this->fight->setCurrentRound($this->fight->getCurrentRound() + 1);
            $this->fight->setRounds($this->fight->getRounds() -1);
        }

        $winner = $this->checkWinner();

        if (is_null($winner)) {
            echo "\n Draw - nu avem nici un castigator";
        } else {
            echo "\n The winner is " . $winner->getName();
        }
    }

    /**
     * @param FighterInterface $hero
     * @param FighterInterface $beast
     * @throws \Exception
     */
    public function setAttackerAndDefender(FighterInterface $hero, FighterInterface $beast): void
    {
        $this->setAttackOrderBySpeed($hero, $beast);
        if (is_null($this->fight->getAttacker())) {
            $this->setAttackerOrderByLuck($hero, $beast);
        }

        if (is_null($this->fight->getAttacker())) throw new \Exception('Nu s-au hotarat');

        return;
    }

    /**
     * @param FighterInterface $hero
     * @param FighterInterface $beast
     */
    private function setAttackOrderBySpeed(FighterInterface $hero, FighterInterface $beast): void
    {
        if ($hero->getSpeed() > $beast->getSpeed()) {
            $this->fight->setAttacker($hero);
            $this->fight->setDefender($beast);
        } elseif ($hero->getSpeed() < $beast->getSpeed()) {
            $this->fight->setAttacker($beast);
            $this->fight->setDefender($hero);
        }

        return;
    }

    /**
     * @param FighterInterface $fighter1
     * @param FighterInterface $fighter2
     */
    public function swapFighters(FighterInterface $fighter1, FighterInterface $fighter2): void
    {
        $this->fight->setAttacker($fighter2);
        $this->fight->setDefender($fighter1);

        return;
    }

    /**
     * @return bool
     */
    public function battleIsActive(): bool
    {
        return ($this->fight->getHero()->isAlive() && $this->fight->getBeast()->isAlive() && $this->fight->getRounds() > 0);
    }

    /**
     *
     */
    public function checkWinner(): ?FighterInterface
    {
        $winner = null;
        if ($this->fight->getHero()->getHealth() <= 0) {
            $winner = $this->fight->getHero();
        }

        if ($this->fight->getBeast()->getHealth() <= 0) {
            $winner = $this->fight->getBeast();
        }

        return $winner;
    }

    /**
     * @param FighterInterface $hero
     * @param FighterInterface $beast
     */
    public function setAttackerOrderByLuck(FighterInterface $hero, FighterInterface $beast): void
    {
        if ($hero->getLuck() > $beast->getLuck()) {
            $this->fight->setAttacker($hero);
            $this->fight->setDefender($beast);
        } elseif ($hero->getLuck() < $beast->getLuck()) {
            $this->fight->setAttacker($beast);
            $this->fight->setDefender($hero);
        }

        return;
    }


    /**
     * @return int
     */
    public function getChance(): int
    {
        return rand(0, 100);
    }


    /**
     * @return bool
     */
    public function avoidAttack(): bool
    {
        $chance = $this->getChance();
        return $this->fight->getDefender()->getLuck() > $chance;
    }


    /**
     * @return void
     */
    private function performAttack(): void
    {
        $this->calculateDamage();

        $skills = $this->fight->getAttacker()->getOffensiveSkills();
        foreach ($skills as $skill) {
            $this->applyOffensiveSkill($skill);
        }
        $this->attackDefender();

        return;
    }


    /**
     * @param SkillInterface $skill
     */
    private function applyOffensiveSkill(SkillInterface $skill): void
    {
        if ($skill->getChance() > $this->getChance()) {
            echo "\n -> {$skill->getName()} was applied";
            switch ($skill) {
                case $skill instanceof RapidStrikeSkill:
                    $this->attackDefender();
                    break;
                case $skill instanceof BestStrikeSkill:
                    $this->fight->setDamageValue($skill->calculateDamageApplied($this->fight->getDamageValue()));
                    break;
                default:
                    break;
            }
        }

        return;
    }

    /**
     * @param SkillInterface $skill
     */
    private function applyDefensiveSkills(SkillInterface $skill): void
    {
        if ($skill->getChance() > $this->getChance()) {
            echo "\n -> {$skill->getName()} was applied";
            switch ($skill) {
                case $skill instanceof MagicShieldSkill:
                    $this->fight->setDamageValue($skill->calculateDamageApplied($this->fight->getDamageValue()));
                    break;
                default:
                    break;
            }
        }

        return;
    }

    /**
     * @return float
     */
    public function calculateDamage(): float
    {
        $this->fight->setDamageValue(
            $this->fight->getAttacker()->getStrength() - $this->fight->getDefender()->getDefense()
        );

        return $this->fight->getDamageValue();
    }

    public function attackDefender(): void
    {
        if ($this->avoidAttack()) {
            $this->log->echoStats(
                $this->fight->getAttacker(),
                $this->fight->getDefender(),
                $this->fight->getDamageValue(),
                'dodge');
            $this->log->echoStats(
                $this->fight->getAttacker(),
                $this->fight->getDefender(),
                $this->fight->getDamageValue(),
                'health');
        } else {
            if ($this->fight->getDefender()->hasSkills()) {
                $defensiveSkills = $this->fight->getDefender()->getDefensiveSkills();
                foreach ($defensiveSkills as $defensiveSkill) {
                    $this->applyDefensiveSkills($defensiveSkill);
                }
            }
            $this->fight->getDefender()->inflictDamage($this->fight->getDamageValue() );
            $this->log->echoStats(
                $this->fight->getAttacker(),
                $this->fight->getDefender(),
                $this->fight->getDamageValue(),
                'attack');
            $this->log->echoStats(
                $this->fight->getAttacker(),
                $this->fight->getDefender(),
                $this->fight->getDamageValue(),
                'health');
        }

        return;
    }
}
