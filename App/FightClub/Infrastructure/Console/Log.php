<?php
/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/16/17
 * Time: 10:28 PM.
 */

namespace FightClub\Infrastructure\Console;


use FightClub\Domain\Model\Fighter\FighterInterface;


/**
 * Class Log
 * @package FightClub\Infrastructure\Console
 *
 * This should be an event sourcing mapping, but its just an echo.
 */
class Log
{
    /**
     * @param FighterInterface $fighter
     */
    public function echoFighter(FighterInterface $fighter): void
    {
        echo "\n\n" . $fighter->getName() . " stats:";
        foreach ($fighter->getFighterStats() as $key => $value) {
            echo "\n" . $key . " => " . $value;
        }
    }

    /**
     * @param FighterInterface $attacker
     * @param FighterInterface $defender
     * @param int $damage
     * @param string $type
     */
    public function echoStats(FighterInterface $attacker, FighterInterface $defender, float $damage, string $type): void
    {
        switch ($type) {
            case 'attack':
                echo "\n" . $attacker->getName() . " attacks " . $defender->getName() . " with a damage of $damage";
                break;
            case 'dodge':
                echo "\n" . $defender->getName() . " dodges the attack";
                break;
            case 'health':
                echo "\n" . $defender->getName() . " has " . $defender->getHealth() . " HP left";
                break;
            case 'damage':
                echo "\n" . $attacker->getName() . " has done $damage damage to " . $defender->getName();
                break;
        }
    }

    /**
     * @param int $round
     */
    public function echoRound(int $round): void
    {
        echo "\n\n******************************************* Round $round";
    }
}
